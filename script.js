document.getElementById("countButton").onclick = function() {  
    
    //estilizando a caixa
    let letterborder = document.getElementById('lettersDiv');
    letterborder.style.border = '2px solid black';
    letterborder.style.padding = '5px';
    letterborder.style.marign= '2px';
    letterborder.style.borderRadius = '5px';

    let wordsborder = document.getElementById('wordsDiv');
    wordsborder.style.border = '2px solid black';
    wordsborder.style.padding = '5px';
    wordsborder.style.marign= '2px';
    wordsborder.style.borderRadius = '5px';
    
    //codigo botão
    let typedText = document.getElementById("textInput").value;
    console.log(typedText)

    typedText = typedText.toLowerCase();
    typedText = typedText.replace(/[^a-z'\s]+/g, "");

    const letterCounts = {};

    let lettersDiv = document.getElementById("lettersDiv")
    let wordsDiv = document.getElementById('wordsDiv')

    let words = {};
    let wordsCounts = {};

    lettersDiv.innerHTML = ''
    wordsDiv.innerHTML = ''

  

    document.getElementById("lettersDiv").innerHTML = typedText;

    //contador de letras
    for (let i = 0; i < typedText.length; i++) {
        currentLetter = typedText[i];

        if (letterCounts[currentLetter] === undefined) {
            letterCounts[currentLetter] = 1; 
         } else { 
            letterCounts[currentLetter]++; 
         }
     }
     
     for (let letter in letterCounts) { 
        const span = document.createElement("span"); 
        const textContent = document.createTextNode(' "' + letter + "\": " + letterCounts[letter] + ", "); 
        span.appendChild(textContent); 
        document.getElementById("lettersDiv").appendChild(span); 
        
    }
        words = typedText.split(/\s/);

    //contador de palavras
    for (let j = 0; j < words.length; j++){
        let currentWords = words[j];

        if (wordsCounts[currentWords] === undefined) {
            wordsCounts[currentWords] = 1
        } else {
            wordsCounts[currentWords]++
        }
    }

     for (let word in wordsCounts) {
         let spanw = document.createElement("spanw");
         let textWords = document.createTextNode(' "' + word + "\": " + wordsCounts[word] + ", ");
         spanw.appendChild(textWords);
         //document.getElementById("wordsDiv").appendChild(spanw);
         wordsDiv.appendChild(spanw);
    }

    

 }